# python-web-scaper
Author: Tim Flucker
Last Modified: 02/03/2020

This project is a web scraper to pull employee data from the HHS employee website and convert that data into a csv document.  This was commissioned by a fellow Atlas Research employee and is in a working state.  

### Author: Tim Flucker
### Last Modified: 02/03/2020

# Process 

- create POST request that mimics form submission at this URL ()
- Create list for all href links in resulting table
 - If more than 500 records (hard limit from site), then resubmit POST request but modify it to get records after the initial 500
 - if only 1 record returned from POST request then only that info will be included on the CSV and all following steps are skipped
- Afer all hrefs collected, navigate to URL with href
 - Find table on page
 - map data in table to CSV record and add to list
- Create CSV document, add each record in CSV data list to this document
- Complete process and return metrics