# Dependencies
# pip install requests, bs4, lxml, csv
import sys
import requests
import bs4
import re
import csv
import lxml
import time

from string import ascii_uppercase, ascii_lowercase
from datetime import datetime, timedelta
from lxml import etree

# initializing global variables
data = list([])

# time_stamps for metrics
csv_time = ''
href_time = ''

# Contains all functions in script
def main():

    global href_time, csv_time

    # used for header record in csv, and in maintaining data consistency
    data_def = list(['Last name', 'First name', 'Middle name', 'Agency', 'Organization',
                 'Job title', 'Non-govt', 'Building', 'Room', 'Duty station', 'Mail stop', 'Phone', 'Fax', 'Internet e-mail'])

     # specification for query provided by client
    agency = 'FDA'
    print('\n---  PROCESS INITIATING  ---\n')
    start_time = datetime.now()
    # send post request to web URL
    # get all links to employee table(s)
    links = query(agency, data_def)

    start_time2 = datetime.now()
    #for every link, get employee table
    for link in links:
        # get the HTML table containing employee data
        table = get_employee_record(link)
        # get data from HTML employee table
        get_employee_data(table, data_def)
        time.sleep(3)
    end_time2 = datetime.now()
    print('All employee data collected ... ')

    # call create csv method
    recordCount = create_csv(data, data_def)
    
    end_time = datetime.now()
    print('\n--- START METRICS ---')
    print('HREF Time elapsed: '.format(href_time))
    print('Get Employee Data Time elapsed: '.format(str(end_time2 - start_time2)))
    print('Create CSV Time elapsed: '.format(csv_time))
    print('Total Time elapsed: '.format(str(end_time - start_time)))
    print('Total Record Count: '.format(recordCount))
    print ('--- END METRICS ---\n')
    print('\n---  PROCESS COMPLETED  ---\n')


# querys site and returns list of href strings
def query(agency, data_def):
    global href_time
    start_time = datetime.now()
    links = list([])
    letters = ascii_uppercase
    # letters2 = ascii_lowercase
    # letters3 = ascii_lowercase
    for first_letter in letters:
        # for second_letter in letters2:
            # for third_letter in letters3:
                lastName = first_letter
                # lastName = first_letter + second_letter + third_letter
                # send post request to return list of records
                page = requests.post('https://directory.psc.gov/hhsdir/eeQ.asp', data={
                    'LastNameOp': 'begins with', 'LastName': lastName, 'AgencyOp': 'equal to', 'Agency': agency, 'OrgCodeOp': 'contains', 'OrgCode': 'CTP','maxRows': '500'})
                soup_obj = bs4.BeautifulSoup(page.text, 'lxml')

                if soup_obj.find(text='No matching records found.'):
                    continue
                elif soup_obj.find(text='HHS Employee Details'):
                    table = soup_obj.find(
                        'table', attrs={'cellspacing': '1', 'cellpadding': '3'})
                    get_employee_data(table, data_def)
                else:
                    # get all href links for record data
                    for link in soup_obj.find_all('a'):
                        ref = link.get('href')
                        if re.match(r'^eeKey.asp', ref):
                            links.append(ref)

    print('All href collected ... ')
    print('Number of href: ',len(links))
    end_time = datetime.now()
    href_time = str(end_time - start_time)
    page.close()
    return links

# get the HTML table of the employee
def get_employee_record(link):
    full_link = 'https://directory.psc.gov/hhsdir/' + link
    print(full_link)
    page = requests.get(full_link)
    soup = bs4.BeautifulSoup(page.text, 'lxml')
    table = soup.find(
        'table', attrs={'cellspacing': '1', 'cellpadding': '3'})
    page.close()
    return table

# get the relevant data from the HTML employee table
def get_employee_data(table, data_def):
    global data
    row_data = list([])
    i = 0
    # iterate through all table cells (contain labels and data)
    while i <= len(data_def)-1:
        # find label in employee table based on data definition (list of all relevant columns)
        label = table.find(text=data_def[i])

        # if lable is found, get relevant data. If label is not found then put in empty space as value
        if label:
            value = table.find(text=data_def[i]).findNext('td').get_text()
        else:
            value = ''
        
        # add value to row data for employee
        row_data.append(value)
        i += 1

    # add array of employee data to master data object
    data.append(row_data)
    return data


# create CSV record containing all employee data
def create_csv(data_for_csv, data_def):
    global csv_time
    recordCount = 0
    start_time = datetime.now()
    # create new csv and write all data into it as rows
    with open('FDA_CTP_results.csv', 'w', newline='') as csvfile:
        # filewriter = csv.writer(csvfile)
        filewriter = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        # Write the column headers row
        filewriter.writerow(data_def)

        # Write row for every employee's data that was collected
        for row in data_for_csv:
            filewriter.writerow(row)
            recordCount += 1
    print('CSV created ...')
    end_time = datetime.now()
    csv_time = str(end_time - start_time)
    return recordCount


# runs the script
main()
